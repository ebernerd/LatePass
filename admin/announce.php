<?php
	require getcwd()."../../php/connect.php";
	if (!isset($_SESSION)) {
		session_start();
	}
	if (!isset($_SESSION['username_latepass'])) {
		header( "Location: ../");
	}
	if (isset($_GET['logout'])) {
		$_SESSION = null;
		session_destroy();
		header( "Location: ../" );
	}

	if (isset($_POST['announcetitle'])) {

	}

	function getClubs() {
		$u = $_SESSION['username_latepass'];
		
	}
?>

<!doctype html>
<html>
	<head>
		<title>New Club | Late Pass</title>

		<link href="../css/admin/announce.css" rel="stylesheet">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
		<script src="../js/metro.min.js"></script>
		<script src="../resources/ckeditor/ckeditor.js"></script>


		<script>
			function updateStuff() {
				if ($("#announcetitle").val().length <= 0) {
					$("#ann_title").text("This is an example announcement.");
				} else {
					$("#ann_title").text($("#announcetitle").val());
				}
				if (CKEDITOR.instances.announcebody.getData().length <= 6) {
					console.log("test");
					$("#ann_body").html("<p>This is your announcement! You can do some cool stuff here.</p><p>You can include <b>bolded text</b>, <i>italicized text</i>, and other formatting.</p>");
				} else {
					$("#ann_body").html(CKEDITOR.instances.announcebody.getData());
				}
			}
			$(document).ready(function() {
				$("html")
					.keyup(updateStuff)
					.keypress(updateStuff)
					.change(updateStuff)
					.click(updateStuff)
				
				$(".title").on("click", function(e) {
					e.preventDefault();
					/*$(this).parent().children(".body").toggle(500);
					if ($(this).parent().children(".body").is(":visible")) {
						$(this).parent().children(".title #more").text("Click here to retract");

					} else {
						$(this).parent().children(".title #more").text("Click here to expand");
					}*/
					var body = $(this).parent().children(".body");
					if (body.hasClass("hidden")) {
						body.removeClass("hidden");
						body.show(500);
						$("html, body").animate({ scrollTop: $(document).height() }, "slow");
						$(this).children("#more").text("Click here to retract");
					} else {
						body.hide(500);
						body.addClass("hidden");
						$(this).children("#more").text("Click here to expand");
					}
				})
				$(".input-icon").click(function(e) {
					$("#ann_icon").attr("class", $(this).val());
				})
				$(".input-type").on("click", function(e){
					var val = $(this).val();
					if (val == "1") {
						$(".announcement").attr("class", "announcement normal");
					} else if (val == "2") {
						$(".announcement").attr("class", "announcement green");
					} else if (val == "3") {
						$(".announcement").attr("class", "announcement yellow");
					} else if (val == "4") {
						$(".announcement").attr("class", "announcement red");
					} else if (val == "5") {
						$(".announcement").attr("class", "announcement blue");
					}
				})
			})
		</script>

	</head>
	<body>
		<?php makeHeader(); ?><br>
		<div class="new">
			<div class="top">
				<h1>New Announcement</h1>
			</div>
			<div class="bottom">
				<form action="announce.php" method="post" name="announce" id="announce">

					<p class="title">Announcement Title</p><p><div class="input-control text"><input type="text" name="announcetitle" maxlength="25" id="announcetitle" autocomplete="off"/></div></p><br>
					<p class="title">Announcement Body (max 500 characters)</p><p><textarea name="announcebody" id="announcebody" form="announce"></textarea></p><br>
					<script>CKEDITOR.replace("announcebody");

		
					CKEDITOR.instances.announcebody.on("key", function(e){
						updateStuff();
						if ((CKEDITOR.instances.announcebody.getData()).length >= 1000) {
							var key = e.data.keyCode;
							if (key === 46 || key === 8) {
								updateStuff();
							} else {
								return false;
							}
						}
					})					
					</script><br>
					<p class="title">Announcement Type</p>
					<p><label class="input-control radio small-check">
					    <input type="radio" name='type' id='type' value='1' class="input-type" checked>
    					<span class="check"></span>
    					<span class="caption">Default</span>
					</label>
					<label class="input-control radio small-check">
					    <input type="radio" name='type' id='type' class="input-type" value='2'>
    					<span class="check"></span>
    					<span class="caption">Green / Success</span>
					</label>
					<label class="input-control radio small-check">
					    <input type="radio" name='type' id='type' class="input-type" value='3'>
    					<span class="check"></span>
    					<span class="caption">Yellow / Important</span>
					</label>
					<label class="input-control radio small-check">
					    <input type="radio" name='type' id='type' class="input-type" value='4'>
    					<span class="check"></span>
    					<span class="caption">Red / Danger</span>
					</label>
					<label class="input-control radio small-check">
					    <input type="radio" name='type' id='type' class="input-type" value='5'>
    					<span class="check"></span>
    					<span class="caption">Blue / Passive</span>
					</label>
					</p>
					<br>
					<p class="title">Announcement Icon</p>
					<div class='icons'>
						<div class='icon'>
							<label class="input-control radio small-check">
							    <input type="radio" name='icon' id='icon' class="input-icon" value='mif-bubbles' checked>
		    					<span class="check"></span>
		    					<span class="caption"><span class='ico'><span class="mif-bubbles"></span></span><br>Bubbles</span>
							</label>
						</div>
						<div class='icon'>
							<label class="input-control radio small-check">
							    <input type="radio" name='icon' id='icon' class="input-icon" value='mif-warning'>
		    					<span class="check"></span>
		    					<span class="caption"><span class='ico'><span class="mif-warning"></span></span><br><span class="ann_ico_name">Warning</span></span>
							</label>
						</div>
						<div class='icon'>
							<label class="input-control radio small-check">
							    <input type="radio" name='icon' id='icon' class="input-icon" value='mif-books'>
		    					<span class="check"></span>
		    					<span class="caption"><span class='ico'><span class="mif-books"></span></span><br><span class="ann_ico_name">Books</span></span>
							</label>
						</div>
						<div class='icon'>
							<label class="input-control radio small-check">
							    <input type="radio" name='icon' id='icon' class="input-icon" value='mif-pencil'>
		    					<span class="check"></span>
		    					<span class="caption"><span class='ico'><span class="mif-pencil"></span></span><br><span class="ann_ico_name">Pencil</span></span>
							</label>
						</div>
						<div class='icon'>
							<label class="input-control radio small-check">
							    <input type="radio" name='icon' id='icon' class="input-icon" value='mif-calculator'>
		    					<span class="check"></span>
		    					<span class="caption"><span class='ico'><span class="mif-calculator"></span></span><br><span class="ann_ico_name">Calculator</span></span>
							</label>
						</div>
						<div class='icon'>
							<label class="input-control radio small-check">
							    <input type="radio" name='icon' id='icon' class="input-icon" value='mif-organization'>
		    					<span class="check"></span>
		    					<span class="caption"><span class='ico'><span class="mif-organization"></span></span><br><span class="ann_ico_name">Organization</span></span>
							</label>
						</div>
						<div class='icon'>
							<label class="input-control radio small-check">
							    <input type="radio" name='icon' id='icon' class="input-icon" value='mif-file-pdf'>
		    					<span class="check"></span>
		    					<span class="caption"><span class='ico'><span class="mif-file-pdf"></span></span><br><span class="ann_ico_name">PDF File</span></span>
							</label>
						</div>
						<div class='icon'>
							<label class="input-control radio small-check">
							    <input type="radio" name='icon' id='icon' class="input-icon" value='mif-file-word'>
		    					<span class="check"></span>
		    					<span class="caption"><span class='ico'><span class="mif-file-word"></span></span><br><span class="ann_ico_name">Word File</span></span>
							</label>
						</div>
						<div class='icon'>
							<label class="input-control radio small-check">
							    <input type="radio" name='icon' id='icon' class="input-icon" value='mif-event-available'>
		    					<span class="check"></span>
		    					<span class="caption"><span class='ico'><span class="mif-event-available"></span></span><br><span class="ann_ico_name">Calendar</span></span>
							</label>
						</div>
						<div class='icon'>
							<label class="input-control radio small-check">
							    <input type="radio" name='icon' id='icon' class="input-icon" value='mif-location'>
		    					<span class="check"></span>
		    					<span class="caption"><span class='ico'><span class="mif-location"></span></span><br><span class="ann_ico_name">Location</span></span>
							</label>
						</div>
					</div>
					<br><br>
					<p class="title">Clubs for this Announcement</p>
					<div class="clubs">
						<?php getClubs(); ?>
					</div>
					<br><br>
					<p class="title">Preview</p>				
					<div class="announcement normal">
						<div class="title">
							<p><span class="mif-bubbles" id="ann_icon"></span>&nbsp; <span id='ann_title'>This is an example announcement.</span></p>
							<p id="more">Click here to expand</p>
						</div>
						<div class="body hidden" id="ann_body">
							<p>This is your announcement! You can do some cool stuff here.</p>
							<p>You can include <b>bolded text</b>, <i>italicized text</i>, and other formatting.</p>
						</div>
					</div>
				</form>
			</div>
			<br>
		</div>
	</body>
</html>