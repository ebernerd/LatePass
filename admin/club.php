<?php
	require getcwd()."../../php/connect.php";
	if (!isset($_SESSION)) {
		session_start();
	}
	if (!isset($_SESSION['username_latepass'])) {
		header( "Location: ../");
	}
	if (isset($_GET['logout'])) {
		$_SESSION = null;
		session_destroy();
		header( "Location: ../" );
	}

	function getInfo() {
		$id = $_GET['id'];
		$conn = connectToDatabase();

		$query = $conn->prepare("SELECT * FROM clubs WHERE id=?");
		$query->bindValue(1, $id);
		$query->execute();
		if ($query->rowCount() > 0) {
			$data = $query->fetch(PDO::FETCH_ASSOC);
			$advs = explode("|",$data['advisors']);
			$query = $conn->prepare( "SELECT color FROM tiles WHERE link=?");
			$query->bindValue(1, "club.php?id=".$id);
			$query->execute();
			$col = $query->fetch(PDO::FETCH_ASSOC);
			if (in_array($_SESSION['username_latepass'], $advs) ){
				echo "<div class='top'>";
				echo "	<h1>Editing \"".$data['name']."\"</h1>";
				echo "</div>";
				echo "<div class='bottom'>";
				echo "  <form name='newform' id='newform' action='updateclub.php' method='post'>";
				echo "	<p class='title'>Name</p>";
				echo "  <p><div class='input-control text'><input type='text' name='clubname' value='".$data['name']."' maxlength='25' autocomplete='off' /></div></p><br>";
				echo "  <p class='title'>Description</p>";
				echo "	<p><textarea id='descr' name='descr' form='newform'>".$data['descr']."</textarea></p><br>";
				echo "	<script>CKEDITOR.replace('descr')</script>";

				//oh no. loading checkboxes. welp, was nice knowing you!

				echo "  <p class='title'>Grades</p>";
				$grades = explode( "|",$data['grades']);
				?>
					<label class="input-control checkbox">
						<input type="checkbox" name="grade[]" value="9" <?php if (in_array("9", $grades)) { echo "checked"; } ?>>
						<span class="check"></span>
						<span class="caption">9th Grade</span>
					</label>
					<label class="input-control checkbox">
						<input type="checkbox" name="grade[]" value="10" <?php if (in_array("10", $grades)) { echo "checked"; } ?>>
						<span class="check"></span>
						<span class="caption">10th Grade</span>
					</label>
					<label class="input-control checkbox">
						<input type="checkbox" name="grade[]" value="11" <?php if (in_array("11", $grades)) { echo "checked"; } ?>>
						<span class="check"></span>
						<span class="caption">11th Grade</span>
					</label>
					<label class="input-control checkbox">
						<input type="checkbox" name="grade[]" value="12" <?php if (in_array("12", $grades)) { echo "checked"; } ?>>
						<span class="check"></span>
						<span class="caption">12th Grade</span>
					</label>
				<?php
				echo "<br><br><p class='title'>Times</p>";
				$days = explode( "|",$data['days']);
				?>
					<label class="input-control checkbox">
						<input type="checkbox" name="day[]" value="tues" <?php if (in_array("tues", $days)) { echo "checked"; } ?>>
						<span class="check"></span>
						<span class="caption">Tuesday</span>
					</label>
					<label class="input-control checkbox">
						<input type="checkbox" name="day[]" value="wed" <?php if (in_array("wed", $days)) { echo "checked"; } ?> >
						<span class="check"></span>
						<span class="caption">Wednesday</span>
					</label>
					<label class="input-control checkbox">
						<input type="checkbox" name="day[]" value="thur" <?php if (in_array("thur", $days)) { echo "checked"; } ?>>
						<span class="check"></span>
						<span class="caption">Thursday</span>
					</label>
				<?php
				echo "<br><br><p class='title'>Days Running</p>";
				$times = explode("|",$data['times']);
				?>

					<label class="input-control checkbox">
						<input type="checkbox" name="times[]" value="3" <?php if (in_array("3", $times)) { echo "checked"; } ?>>
						<span class="check"></span>
						<span class="caption">3:15pm</span>
					</label>
					<label class="input-control checkbox">
						<input type="checkbox" name="times[]" value="4" <?php if (in_array("4", $times)) { echo "checked"; } ?>>
						<span class="check"></span>
						<span class="caption">4:15pm</span>
					</label>
					<label class="input-control checkbox">
						<input type="checkbox" name="times[]" value="sport" <?php if (in_array("sport", $times)) { echo "checked"; } ?>>
						<span class="check"></span>
						<span class="caption">Sports Bus</span>
					</label>
					<input type="checkbox" name="id" value="<?php echo $_GET['id']; ?>" checked hidden>

				<?php
				echo "<br><br><h2>Other Settings</h2>";
				echo "<p class='title'>Colour</p>";
				echo "<p>Choose a colour for your club's title!</p><br>";
				?>
				<div class="colour" style="background: #2196F3;">
					<label class="input-control radio">
					    <input type="radio" name='col' id='col' value='2196F3' <?php if ($col['color'] == "2196F3") { echo "checked"; } ?>>
    					<span class="check"></span>
    					<span class="caption"></span>
					</label>
				</div>
				<div class="colour" style="background: #3F51B5;">
					<label class="input-control radio">
					    <input type="radio" name='col' id='col' value='3F51B5' <?php if ($col['color'] == "3F51B5") { echo "checked"; } ?>>
    					<span class="check"></span>
    					<span class="caption"></span>
					</label>
				</div>
				<div class="colour" style="background: #673AB7;">
					<label class="input-control radio">
					    <input type="radio" name='col' id='col' value='673AB7' <?php if ($col['color'] == "673AB7") { echo "checked"; } ?>>
    					<span class="check"></span>
    					<span class="caption"></span>
					</label>
				</div>
				<div class="colour" style="background: #E91E63;">
					<label class="input-control radio">
					    <input type="radio" name='col' id='col' value='E91E63' <?php if ($col['color'] == "E91E63") { echo "checked"; } ?>>
    					<span class="check"></span>
    					<span class="caption"></span>
					</label>
				</div>
				<div class="colour" style="background: #F44336;">
					<label class="input-control radio">
					    <input type="radio" name='col' id='col' value='F44336' <?php if ($col['color'] == "F44336") { echo "checked"; } ?>>
    					<span class="check"></span>
    					<span class="caption"></span>
					</label>
				</div>
				<div class="colour" style="background: #9C27B0;">
					<label class="input-control radio">
					    <input type="radio" name='col' id='col' value='9C27B0' <?php if ($col['color'] == "9C27B0") { echo "checked"; } ?>>
    					<span class="check"></span>
    					<span class="caption"></span>
					</label>
				</div>
				<div class="colour" style="background: #43A047;">
					<label class="input-control radio">
					    <input type="radio" name='col' id='col' value='43A047' <?php if ($col['color'] == "43A047") { echo "checked"; } ?>>
    					<span class="check"></span>
    					<span class="caption"></span>
					</label>
				</div>
				<div class="colour" style="background: #EF6C00;">
					<label class="input-control radio">
					    <input type="radio" name='col' id='col' value='EF6C00' <?php if ($col['color'] == "EF6C00") { echo "checked"; } ?>>
    					<span class="check"></span>
    					<span class="caption"></span>
					</label>
				</div>

				<?php
				/*echo "<p class='title'>Advisors</p>";
				echo "<div class='input-control text'><input type='text' name='_search' id='_search' placeholder='enter an advisors name' autocomplete='off'/></div><div id='result'></div>";
				foreach ($advs as $a) {
					if ($a != $_SESSION['username_latepass']) {
						?>
						<label class="input-control checkbox">
						<input type="checkbox" name="adv[]" value="<?php echo $a; ?>" checked>
						<span class="check"></span>
						<span class="caption"><?php echo $a; ?></span>
						</label>
						<?php
					}
				}
				if ((count($advs))-1 === 0) {
					echo "<p style='color:gray;'>No Other Advisors.</p>";
				}

				*/
				echo "<br><br><p class='title'>Dangerous Settings</p>";
				echo "<input type='submit' value='Delete Club/Activity' name='del' class='del'>";
				echo "<p>You cannot undo a deletion. Only click if you are 100% sure.";
				echo "<br><br><br><input type='submit' value='Save Changes' class='save'/>";
				echo "  </form>";
				echo "</div>";
				echo "<br>";
			} else {
				//This advisor does not have permission to view this.
			}

		} else {
			//fake/invalid id was put in, redirect to main page.
			header("Location: ./");
		}
	}


?>

<!doctype html>
<html>

	<head>
		<title>Update Club | Late Pass</title>


		<script>
		</script>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
		<script src="../js/metro.min.js"></script>
		<script src="../resources/ckeditor/ckeditor.js"></script>
		<script src="../js/club.js"></script>

		<link href="../css/admin/club.css" rel="stylesheet">

	</head>
	<body>
		<?php makeHeader(); ?>
		<div class="club">
			<?php getInfo(); ?>
		</div>
	</body>
</html>