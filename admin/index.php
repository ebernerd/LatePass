<?php
	require getcwd()."../../php/connect.php";
	if (!isset($_SESSION)) {
		session_start();
	}
	if (!isset($_SESSION['username_latepass'])) {
		header( "Location: ../");
	}
	if (isset($_GET['logout'])) {
		$_SESSION = null;
		session_destroy();
		header( "Location: ../" );
	}


	function getTiles() {
		$conn = connectToDatabase();

		$query = $conn->prepare("SELECT * FROM tiles WHERE user=? ORDER BY user ASC LIMIT 10");
		$query->bindValue(1, $_SESSION['username_latepass']);
		$query->execute();
		echo "<div class='dashtable'>";
		if ($query->rowCount() > 0) {
			$tiles = $query->fetchAll(PDO::FETCH_ASSOC);
			if (count($tiles) > 0) {
				foreach( $tiles as $t ) {
					?>
					<a href="<?php echo$t['link']; ?>">
						<div class="tile">
				    			<div class="tile-content slide-up" style="background: #<?php echo $t['color']; ?>; color: white;">
					        		<div class="slide">
									<div>
				            				<p><?php echo $t['label']; ?></p>
									</div>
				        			</div>
				        			<div class="slide-over" style="background: #DEDEDE">
				          			<div>
										<p>Click here to configure</p>
									</div>
				        			</div>
				    			</div>
						</div>
					</a>
					<?php
				}
			}
		} else {
			echo "<p class='noclub'>You don't have any clubs! Perhaps you should <a href='new.php'>create one</a>!</p>";
		}
		echo "</div>";
		$conn = null;
	}

?>

<!doctype html>
<html>
	<head>
		<title>Home | Late Pass</title>

		<link href="../css/admin/style.css" rel="stylesheet">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
		<script src="../js/metro.min.js"></script>

	</head>
	<body>
		<?php makeHeader(); ?>
		<div class="dashboard">
			<h1>My Dashboard&nbsp;<small>Click a club's tile to configure it</small></h1><br>
			<div class="inner-tiles">
				<div>
					<?php getTiles(); ?>

					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</body>
</html>
