<?php
	require getcwd()."../../php/connect.php";
	if (!isset($_SESSION)) {
		session_start();
	}
	if (!isset($_SESSION['username_latepass'])) {
		header( "Location: ../");
	}
	if (isset($_GET['logout'])) {
		$_SESSION = null;
		session_destroy();
		header( "Location: ../" );
	}
	if (isset($_POST['clubname'])) {
		$n = $_POST['clubname'];
		$descr = $_POST['descr'];
		$grades = implode("|",$_POST['grades']);
		$days = implode("|",$_POST['days']);
		$times = implode("|",$_POST['times']);
		$cols = array(1 => "2196F3", 2 => "3F51B5", 3 => "673AB7", 4 => "E91E63", 5 => "F44336", 6 => "9C27B0", 7 => "43A047", 8 => "EF6C00");
		$col = $cols[array_rand($cols)];
		$conn = connectToDatabase();
		$query = $conn->prepare("SELECT * FROM clubs WHERE name=? AND advisors LIKE %?%");
		$query->bindValue(1, $n);
		$query->bindValue(2, $_SESSION['username_latepass']);
		$query->execute();
		if ($query->rowCount() > 0) {
			//club exists
		} else {
			$hasID = false;
			$id = "";
			do {
				$id = str_rand(8,"alphanum");
				$query = $conn->prepare("SELECT * FROM clubs WHERE id=?");
				$query->bindValue(1, $id);
				$query->execute();
				if ($query->rowCount() > 0) {
					//Found an identical id, can't have that
				} else {
					$hasID = true;
				}
			} while ($hasID === false);
			$query = $conn->prepare("INSERT INTO clubs (name, descr, grades, times, days, advisors, id) VALUES (?,?,?,?,?,?,?)");
			$query->bindValue(1, $n);
			$query->bindValue(2, $descr);
			$query->bindValue(3, $grades);
			$query->bindValue(4, $times);
			$query->bindValue(5, $days);
			$query->bindValue(6, $_SESSION['username_latepass']);
			$query->bindValue(7, $id);
			//echo "INSERT INTO clubs (names, descr, grades, times, days, advisors, id) VALUES ($n, $descr, $grades, $times, $days, $_SESSION[username_latepass], $id)";
			$query->execute();
			//now that the club is created, we need to initialize a tile for it.
			$query = $conn->prepare("INSERT INTO tiles (user, color, link, type, label) VALUES (?,?,?,?,?)");
			$query->bindValue(1, $_SESSION['username_latepass']);
			$query->bindValue(2, $col);
			$query->bindValue(3, "club.php?id=".$id);
			$query->bindValue(4, 0);
			$query->bindValue(5, $n);
			$query->execute();
			header("Location: ./");
		}
	}
?>

<!doctype html>
<html>
	<head>
		<title>New Club | Late Pass</title>

		<link href="../css/admin/new.css" rel="stylesheet">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
		<script src="../js/metro.min.js"></script>
		<script src="../resources/ckeditor/ckeditor.js"></script>

	</head>
	<body>
		<?php makeHeader(); ?>
		<br>
		<div class="new">
			<div class="top">
				<h1>Make a new club / activity</h1>
			</div>
			<div class="bottom">
				<form action="new.php" method="post" id="newform" name="newform">
					<p class="title">Name</p><p><div class="input-control text"><input type="text" name="clubname" maxlength="25" autocomplete="off" /></div></p><br>
					<p class="title">Description</p><p><textarea id="descr" name="descr" form="newform"></textarea></p><br>
					<script>CKEDITOR.replace("descr")</script>
					<p class="title">Grades</p><p>
						<label class="input-control checkbox">
							<input type="checkbox" name="grades[]" value="9">
							<span class="check"></span>
							<span class="caption">9th Grade</span>
						</label>
						<label class="input-control checkbox">
							<input type="checkbox" name="grades[]" value="10">
							<span class="check"></span>
							<span class="caption">10th Grade</span>
						</label>
						<label class="input-control checkbox">
							<input type="checkbox" name="grades[]" value="11">
							<span class="check"></span>
							<span class="caption">11th Grade</span>
						</label>
						<label class="input-control checkbox">
							<input type="checkbox" name="grades[]" value="12">
							<span class="check"></span>
							<span class="caption">12th Grade</span>
						</label>
					</p><br>
					<p class="title">Days Running</p><p>
						<label class="input-control checkbox">
							<input type="checkbox" name="days[]" value="tues">
							<span class="check"></span>
							<span class="caption">Tuesday</span>
						</label>
						<label class="input-control checkbox">
							<input type="checkbox" name="days[]" value="wed">
							<span class="check"></span>
							<span class="caption">Wednesday</span>
						</label>
						<label class="input-control checkbox">
							<input type="checkbox" name="days[]" value="thur">
							<span class="check"></span>
							<span class="caption">Thursday</span>
						</label>
					</p><br>
					<p class="title">Times Running</p><p>
						<label class="input-control checkbox">
							<input type="checkbox" name="times[]" value="3">
							<span class="check"></span>
							<span class="caption">3:15pm</span>
						</label>
						<label class="input-control checkbox">
							<input type="checkbox" name="times[]" value="4">
							<span class="check"></span>
							<span class="caption">4:15pm</span>
						</label>
						<label class="input-control checkbox">
							<input type="checkbox" name="times[]" value="sport">
							<span class="check"></span>
							<span class="caption">Sports Bus</span>
						</label>
					</p><br>
					<input type="submit" value="Create Club!" />
				</form>
			</div>
		</div>
		<br>
	</body>
</html>
