<?php
	require getcwd()."../../php/connect.php";
	if (!isset($_SESSION)) {
		session_start();
	}
	if(isset($_POST['clubname'])) {
		//update club info!

		$id = $_POST['id'];

		if (isset($_POST['Delete Club/Activity']) || isset($_POST['del'])) {
			$id = $_POST['id'];
			$conn = connectToDatabase();
			$query = $conn->prepare("DELETE FROM clubs WHERE id=?");
			$query->bindValue(1, $id);
			$query->execute();
			$query = $conn->prepare("DELETE FROM tiles WHERE link=?");
			$query->bindValue(1, "club.php?id=".$id);
			$query->execute();
			header("Location: ./");
		} else {
			$n = $_POST['clubname'];
			$descr = $_POST['descr'];
			$grades = implode("|",$_POST['grade']);
			$day = implode("|",$_POST['day']);
			$times = implode("|",$_POST['times']);
			$color = $_POST['col'];
			$conn = connectToDatabase();
			$query = $conn->prepare("UPDATE clubs SET name=?, descr=?, grades=?, times=?, days=?, advisors=? WHERE id=?");
			$query->bindValue(1,$n);
			$query->bindValue(2, $descr);
			$query->bindValue(3, $grades);
			$query->bindValue(4, $times);
			$query->bindValue(5, $day);
			$query->bindValue(6, $_SESSION['username_latepass']);
			$query->bindValue(7, $id);
			$query->execute();
			//TILES
			$query = $conn->prepare("UPDATE tiles SET label=?, color=? WHERE link=?");
			$query->bindValue(1, $n);
			$query->bindValue(2, $color);
			$query->bindValue(3, "club.php?id=".$id);
			$query->execute();
			header("Location: ./");
		}
	} else {
		//header("Location: ./");
	}
?>