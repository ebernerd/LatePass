<?php
	require getcwd()."../php/connect.php";
	if (!isset($_SESSION)) {
		session_start();
	}
	if (isset($_POST['username'])) {
		$u = $_POST['username'];
		$p = $_POST['password'];
		$conn = connectToDatabase();

		$query = $conn->prepare("SELECT * FROM users WHERE username=?");
		$query->bindValue(1, $u);
		$query->execute();
		if ($query->rowCount() > 0) {
			$data = $query->fetch(PDO::FETCH_ASSOC);
			$newpass = crypt($p, $data['salt']);
			if ($newpass === $data['password']) {
				$_SESSION['username_latepass'] = $u;
				$_SESSION['firstname_latepass'] = $data['first'];
				$_SESSION['lastname_latepass'] = $data['last'];
				$_SESSION['type_latepass'] = $data['type'];
				if ($_SESSION['type_latepass'] == 0) {
					header("Location: admin/");
				} else {
					header("Location: home/");
				}
			} else {
				global $error;
				$error = "Username or password is incorrect.";
			}
		} else {
			global $error;
			$error = "Username or password is incorrect.";
		}

		$conn = null;
	}
	if (isset($_SESSION['username_latepass'])) {
		if ($_SESSION['type_latepass'] == 0) {
			header("Location: admin/");
		} else {
			header("Location: home/");
		}
	}
?>

<!doctype html>
<html>
	<head>
		<title>Late Pass</title>

		<link rel="stylesheet" type="text/less" href="css/index.less" />

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.6.1/less.min.js"></script>

	</head>
	<body>
		<div class="inner">
			<div class="login">
				<div class="top">
					<h1>Log In</h1>
				</div>
				<div class="bottom">
					<?php
						if ( isset($error) ) {
							echo "<div class='error'><p style='color:red;'>$error</p></div><br>";
						}
					?>
					<form name="login" id="login" action="index.php" method="post">
						<p>Username or ID<br><input type="text" name="username" placeholder="username" required autocomplete="off"></p><br>
						<p>Password<br><input type="password" name="password" placeholder="password" required autocomplete="off"></p><br>
						<p><input type="submit" value="Log In"></p><br>
						<p><a href="#">Don't have an account?</a></p>
						<div class="spacer"></div>
					</form>
				</div>
			</div>
		</div>
		<footer>
			<a href="#">Support</a>
			<a href="#">Inquiries</a>
			<a href="#">Terms of Service</a>
		</footer>
	</body>
</html>
