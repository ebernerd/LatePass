function adjustPage() {
	if ($(window).width()*0.20 > 300) {
		$(".page").width( $(window).width()*0.80 - 50)
	} else {
		$(".page").width( $(window).width()-350);
	}
}

$(document).ready(function() {

	function smoothScroll(amt) {
		$('html, body').animate({ scrollTop: amt }, 'slow');
	}


	function getPageContents( link ) {

		smoothScroll(0);
		$.ajax({
			url: link,
			method: "GET",
		}).done(function(data) {
			$(".page").empty();
			$(".page").html( data );
		})
	}

	function test() {
		console.log("hellyea");
	}




	$(".dashboard .tile").each(function(i, element) {
		var dataw = $(element).attr("data-w");
		var datah = $(element).attr("data-h");
		var datac = $(element).attr("data-c");
		var unit = 90;
		$(element).width( Math.floor(dataw * unit) );
		$(element).height( Math.floor(datah * unit) );
		$(element).css("background", "#"+datac);
	});
	$(".dashboard").height( $(window).height()-55 );
	adjustPage();
	$(window).resize(function() {
		$(".dashboard").height( $(window).height()-95 );
		adjustPage();
	});
	$("#newclubbutton").click(function() {
		getPageContents("../pages/makeclub.php");
		adjustPage();
	});
	$('html').on('contextmenu', function(e) {
	});
	$('html').on("click", function(e) {
		$("#contextmenu").css("display", "none");
	});
	$(".tile").on("contextmenu", function(e) {
		e.preventDefault();
		console.log("clicking " + (e.pageX-10) + " " + (e.pageY-10));
		var ctxMenu = document.getElementById("contextmenu");
	     ctxMenu.style.display = "block";
	     ctxMenu.style.left = (event.pageX - 10)+"px";
	     ctxMenu.style.top = (event.pageY - 10)+"px";
	})
	$(".tilewrapper").on("click", function(e) {
		e.preventDefault();
		$(".page").empty();
		console.log( $(this).attr("data-l") );
		console.log("'../pages/"+$(this).attr("data-l"));
		getPageContents( '../pages/'+$(this).attr("data-l") );

		adjustPage();
	});
	$("#homebutton").on("click", function(e) {
		e.preventDefault();
		getPageContents("../pages/main.php");
	})

	getPageContents("../pages/main.php");


	$(".page").on("click", "a", function(e) {
		if ($(this).attr('target') != "_blank") {
			e.preventDefault();
			getPageContents($(this).attr('data-href'));
		}
	});
})
