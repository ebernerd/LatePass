
$(document).ready(function() {
	
	$("#_search").keyup(function() {
		var searchid = $(this).val();
		var dataString = "search="+searchid;
		if (searchid!="") {
			$.ajax({
				type: "POST",
				url: "../php/ls.php",
				data: dataString,
				cache: false,
				success: function( html ) {
					$("#result").html(html).show();
					$("#result").css("border", "1px solid gray");
				}
			});
		} else {
			$("#result").empty();
			$("#result").css("border", "none");
		}
	})
});