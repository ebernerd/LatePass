<?php
	require getcwd()."../php/connect.php";


	if (!isset($_SESSION)) {
		session_start();
	}
	if (isset($_POST['username'])) {
		$u = $_POST['username'];
		$p1 = $_POST['password1'];
		$p2 = $_POST['password2'];
		$salt = substr(str_shuffle("./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345‌​6789"), 0, 8);
		if ($p1 === $p2) {
			$hashed = crypt($p1, $salt);
			$conn = connectToDatabase();

			$query = $conn->prepare("SELECT * FROM users WHERE username=?");
			$query->bindValue(1, $u);
			$query->execute();
			if ($query->rowCount() > 0) {
				global $error;
				$error = "A user with that name already exists.";
			} else {
				$query = $conn->prepare("INSERT INTO users (username, password, salt, first, last, grade, type) VALUES (?,?,?,?,?,?,?)");
				$query->bindValue(1, $u);
				$query->bindValue(2, $hashed);
				$query->bindValue(3, $salt);
				$query->bindValue(4, "");
				$query->bindValue(5, "");
				$query->bindValue(6, 0);
				$query->bindValue(7, 0);
				$query->execute();
				header("Location:./");

			}

			$conn = null;
		} else {
			global $error;
			$error = "A user with that name already exists.";
		}
	}
?>

<!doctype html>
<html>
	<head>
		<title>Late Pass</title>

		<link rel="stylesheet" type="text/less" href="css/index.less" />

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.6.1/less.min.js"></script>

	</head>
	<body>
		<div class="inner">
			<div class="login">
				<div class="top">
					<h1>Create User</h1>
				</div>
				<div class="bottom">
					<?php
						if ( isset($error) ) {
							echo "<div class='error'><p style='color:red;'>$error</p></div><br>";
						}
					?>
					<form name="mkuser" id="mkuser" action="makeuser.php" method="post">
						<p>Username or ID<br><input type="text" name="username" placeholder="username" required autocomplete="off"></p><br>
						<p>Password<br><input type="password" name="password1" placeholder="password" required autocomplete="off"></p><br><input type="password" name="password2" placeholder="repeat password" required autocomplete="off"></p><br></p>
						<p><input type="submit" value="Create User"></p><br>

						<div class="spacer"></div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
