<?php
	function connectToDatabase() {
		return new PDO( "mysql:host=127.0.0.1;dbname=latepass;", "root", "" );
	}

	function str_rand($length = 8, $seeds = 'alphanum') {
		// Possible seeds
		$seedings['alpha'] = 'abcdefghijklmnopqrstuvwqyz';
		$seedings['numeric'] = '0123456789';
		$seedings['alphanum'] = 'abcdefghijklmnopqrstuvwqyz0123456789';
		$seedings['hexidec'] = '0123456789abcdef';

		// Choose seed

		if (isset($seedings[$seeds])) {
			$seeds = $seedings[$seeds];
		}

		// Seed generator
		list($usec, $sec) = explode(' ', microtime());
		$seed = (float) $sec + ((float) $usec * 100000);
		mt_srand($seed);

		// Generate
		$str = '';
		$seeds_count = strlen($seeds);

		for ($i = 0; $length > $i; $i++) {
			$str .= $seeds{mt_rand(0, $seeds_count - 1)};
		}

		return $str;
	}
	function adjustBrightness($hex, $steps) {
	    // Steps should be between -255 and 255. Negative = darker, positive = lighter
	    $steps = max(-255, min(255, $steps));

	    // Normalize into a six character long hex string
	    $hex = str_replace('#', '', $hex);
	    if (strlen($hex) == 3) {
	        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
	    }

	    // Split into three parts: R, G and B
	    $color_parts = str_split($hex, 2);
	    $return = '#';

	    foreach ($color_parts as $color) {
	        $color   = hexdec($color); // Convert to decimal
	        $color   = max(0,min(255,$color + $steps)); // Adjust color
	        $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
	    }

	    return $return;
	}
	function makeHeader(){
		?>
		<ul class="horizontal-menu" id="header">
			<li><a>LATE PASS</a></li>
			<li>&nbsp;&nbsp;&nbsp;</li>
			<li><a href="./">Home</a></li>
		    <li>
		        <a href="#" class="dropdown-toggle">Create...</a>
		        <ul class="d-menu" data-role="dropdown">
		            <li><a href="new.php"><span class="mif-plus"></span>&nbsp;Club / Activity</a></li>
		            <li><a href="announce.php"><span class="mif-bubbles"></span>&nbsp;Announcement</a></li>
		            <li><a href="#"><span class="mif-user-plus"></span>&nbsp;User</a></li>
		        </ul>
		    </li>
		    <li><a href="#">My Profile</a></li>
		    <li><a href="?logout">Log Out</a></li>
		</ul>
		<br>
		<?php
	}
?>
