<?php
if (!isset($_SESSION)) {
	session_start();
}
if (isset($_GET['logout'])) {
	$_SESSION = null;
	session_destroy();
	header( "Location: ./" );
}
?>

<!doctype html>
<html>
	<head>
		<title>Support | Late Pass</title>

		<link rel="stylesheet" type="text/less" href="css/support.less">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

		<script>

			$(document).ready(function(){
				$(".content").width( $(window).width() - 485 );
				$(".cover").width( $(window).width() - 300 );
				$(window).resize(function() {
					$(".content").width( $(window).width() - 360 );
					$(".cover").width( $(window).width() - 300 );
				})

				$(".item").on("click", function(e) {
					e.preventDefault();
					if ($(this).next(".submenu").length > 0) {
						$(this).next(".submenu").toggle(100);
					} else {
						var href = $(this).attr("href");
						console.log($(href).offset());
						console.log($(href));
						$('html,body').scrollTop( $(href).offset().top );
					}
				})

				$(".submenu").each(function(e) {
					$(this).hide();
				})
			})

		</script>

		<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.6.1/less.min.js"></script>
	</head>
	<body>


		<div class="cover">
			<div>
				<h1>Late Pass Support</h1><br>
				<a href="./"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Return to homepage</a>
			</div>
		</div>
		<div class="content">
			<h1>FAQs</h1>
			<hr><br>
			<h2 id="whatis">What is Late Pass?</h2>
			<p>Late Pass is a system that allows teachers and students to communicate better in the process of managing buses. In many schools, the bussing systems for extra-curricular activities is quite hectic, and can leave a lot of students scrambling to get the required passes and get to their bus. Late Pass aims to solve this by digitializing and centralizing the systems that run this bussing system.</p><br>
			<h2 id="whyuse">Why use Late Pass?</h2>
			<p>Late Pass is a great option for connecting teachers and students, and making many co-dependent systems work together in one package. Late Pass includes features such as:<br>
				<ul><li>Teacher-administered clubs and activities</li><li>Student subscription to frequently visited clubs</li><li>Multi-advisor clubs and activities</li><li>Announcements for students who are subscribed to your clubs</li></ul>...and many more!</p><br>
			<br>
			<h1>Administrator Help</h1>
			<hr><br>
			<h2 id="overview">Basic Overview</h2>

		</div>
		<div class="menu">
			<br>
			<h2>Support Items</h2><br><hr><br>
			<div class="menusection">
				<a class="item" href="#">FAQ</a>
				<div class="submenu">
					<a class="item" href="#whatis">What is Late Pass?</a>
					<a class="item" href="#whyuse">Why use Late Pass?</a>
				</div>
				<a class="item" href="#">Administrator Help</a>
				<div class="submenu">
					<a class="item" href="#overview">Basic Overview</a>
				</div>
			</div>
		</div>

	</body>
</html>
